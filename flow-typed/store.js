

declare type Action<Attrs = {}> = {|
  +type: string,
  +payload:Attrs
|}
