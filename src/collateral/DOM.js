
import { element, update } from 'element-queries'

const SYMBOLS_ELEMENT_DESCRIPTOR = 'div.icon-vector-src'


/**
 * Append the div.icon-vector-src element to the DOM
 * @exports {Function} appendSymbolsElement
 */

export const appendSymbolsElement = (content) =>
  appendElement(SYMBOLS_ELEMENT_DESCRIPTOR, { content })

export const getSymbolsElement = () =>
  element.get(SYMBOLS_ELEMENT_DESCRIPTOR)


/**
 * Append the style.preview-styles style element to the DOM
 * @exports {Function} appendPreviewStyleElement
 */

export const appendPreviewStyleElement = (data) =>
  appendStylesheetElement('preview-styles', data)


/**
 * Helper for appending a css stylesheet element
 */
const appendStylesheetElement = (name, styles) =>
  appendElement(`style.${name}`, {
    content: toCSS(styles),
    type: 'text/css'
  })

/**
 * Display an overlay element for an error message
 * @exports {Function} showExceptionElement
 */

export const showExceptionElement = (error) =>
  appendElement('div.global-error', {
    content: `
      <h2>Error</h2>
      <p>${error.message}</p>`
  })

export const applyGlobalAttribute = (attribute, value) =>
  update(document.documentElement, { [attribute]: value })

export const applyGlobalProperty = (attribute, value) =>
  applyGlobalAttribute(`data-${attribute}`, value)

/**
 * Append an element to the end of the body element.
 *
 * @function       appendElement
 * @param {string} descriptor    The tag for the element
 * @param {string} attributes
 * @return {HTMLElement}         An HTML element of type @param.tagName
 *                               with @param.className as its class attribute
 *                               and @param.content as its innerHTML
 *
 * @example                      <[tagName] class='[className]'>
                                   [content]
                                 </[tagName]>
 */

export function appendElement (descriptor, attributes = {}) {

  // Find an existing or create a new
  let node = element.getOrCreate(descriptor)

  // Set/update the element's attributes
  update(node, attributes)

  // Append to the DOM
  return document.body.appendChild(node)
}

/**
 * Prepend an element to the start of the body element.
 *
 * @function       prependElement
 * @param {string} descriptor    The tag for the element
 * @param {string} attributes
 * @return {HTMLElement}         An HTML element of type @param.tagName
 *                               with @param.className as its class attribute
 *                               and @param.content as its innerHTML
 */

export function prependElement (descriptor, attributes = {}) {

  // Find an existing or create a new
  let node = element.getOrCreate(descriptor)

  // Set/update the element's attributes
  update(node, attributes)

  // Insert to the DOM
  const next = document.body.firstElementChild
  if (next === null)
    return document.body.appendChild(node)
  return document.body.insertBefore(node, next)
}

/**
 * Rasterize the given object with selectors as its keys & objects consisting
 * of respective css rules into a string of valid css.
 *
 * @function toCSS
 *
 * @param   {Object} styles An object consisting of a selector (key); and respective attributes (value)
 *                          from which the resulting css is generated from.
 * @returns {String}        A valid CSS string based upon the given style attributes.
 */

function toCSS (styles = {}) {
  const chunks = []

  for (let selector in styles) {
    chunks.push(`${selector} {`)

    for (let attribute in styles[selector])
      chunks.push(`  ${attribute}: ${styles[selector][attribute]};`)

    chunks.push(`}`)
  }
  return chunks.join('\n')
}
