// @flow
// @flow-runtime
import self from 'autobind-decorator'
import React, { Component } from 'react'
import { CompositeDisposable } from 'event-kit'

import List from './views/IconsList'
import Options from './views/OptionsView'
import SVGView from './views/SVGSourceView'
import scrollToPosition from './utils/Scroll'
import { SettingsProvider } from './models/settings'
import { url } from './api'
// import { addListener, withDefaultPrevented } from './utils/events'


import './styles/main.less'


const scrollDown = () =>
  window.scrollY < window.innerHeight &&
  scrollToPosition(window.innerHeight)


const ScrollDownIndicator = () =>
  <span
    className='icon'
    onClick={ scrollDown }>
    skrol
  </span>


export default class Application extends Component<{}, { svgOverlayVisible: boolean }> {

  subscriptions: CompositeDisposable

  state = {
    svgOverlayVisible: false
  }

  // componentDidMount () {
  //
  //   const onKey = () => {
  //     scrollDown()
  //     subscription.dispose()
  //   }
  //   const subscription = addListener(document, 'keydown', withDefaultPrevented(onKey))
  //   this.subscriptions = new CompositeDisposable()
  //   this.subscriptions.add(
  //     subscription,
  //   )
  // }
  //
  // componentWillUnmount () {
  //   this.subscriptions.dispose()
  // }

  @self
  toggleSVGOverlay () {
    const svgOverlayVisible = !this.state.svgOverlayVisible
    this.setState({ svgOverlayVisible })
  }

  render () {
    return <SettingsProvider>

      <header className='page'>
        <div>
          <h1>Trinity Icons</h1>
          <sub>An all-new open-source icon set in SVG, webfont and image formats.</sub>
          <ScrollDownIndicator />
        </div>
      </header>

      <main className='page'>

        <section className='actions'>
          <a className='display button' onClick={ this.toggleSVGOverlay }>Svg</a>
          <a className='display button' href={ url.webfont }>Webfont</a>
          <a className='display button' href={ url.images }>Images</a>
        </section>

        <Options />

        <List />

      </main>

      <footer>
      </footer>

      <SVGView
        onClose={ this.toggleSVGOverlay }
        visible={ this.state.svgOverlayVisible } />
    </SettingsProvider>
  }

}
