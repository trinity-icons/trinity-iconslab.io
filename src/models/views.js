// @flow
// @flow-runtime
import { createElement } from 'react'
import type { Node, Element } from 'react'

type AnyClass = Class<*>
type Component = Element<*>
type ModelType = {
  +constructor: AnyClass
}


class ViewsRegistry {

  providers:  WeakMap<AnyClass, Component>

  constructor () {
    this.providers = new WeakMap()
  }

  register (model: AnyClass, view: Component) {
    this.providers.set(model, view)
  }

  get (instance: ModelType): Node {
    const model = instance.constructor

    if (!this.providers.has(model))
      return null

    return createElement(
      this.providers.get(model),
      instance
    )
  }

}

export default new ViewsRegistry()
