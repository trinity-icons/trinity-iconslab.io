// @flow
// @flow-runtime


export type TypeEnum = 'boolean' | 'number' | 'color'


export type SettingType = {|
  +type: TypeEnum,
  +name: string,
  +title: string,
  +description?: string,
|}


const props = Symbol('Properties')


export default class SettingProperty {

  constructor ({ type, name, title, description }) {
    (this[props]: SettingType) = {
      type,
      name,
      title,
      description,
    }
  }
}
