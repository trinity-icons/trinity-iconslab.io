// @flow
// @flow-runtime
import views from './views'
import Icon from '../components/Icon'

export type VariantType = 'filled' | 'outline' | 'regular'

export type IconType = {|
  +id?: string,
  +name: string,
  +displayName: string,
  +shortDisplayName: string,
  +variant: VariantType,
  +category: string,
  +path: string,
  +date: string,
  +title: string,
  +raster: boolean,
|}


export default class IconDescription {

  constructor (data: IconType) {
    Object.assign(this, data)
  }

  static isRegular (icon: IconDescription) {
    return icon.variant === 'regular'
  }
}

views.register(IconDescription, Icon)
