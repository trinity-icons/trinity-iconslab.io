// @flow
// @flow-runtime

import React, { createContext, createRef, Component } from 'react'
import preview from '../utils/Preview'


const { Provider, Consumer } = createContext({})


export const DEFAULTS = {
  iconStrokeWidth: 1.5,
  showLabels:      false,
  iconSize:        8,
  iconColor:       '#4b505a',
  sortBy:          'title',
}

export const DEFAULT_PROPERTIES = Object.entries(DEFAULTS)


export class SettingsProvider extends Component {

  state = DEFAULTS

  setOptionValue (key, value) {

    this.setState({ [key]: value }, () =>
      preview.update(this.state)
    )
  }

  getInterface () {

    const set = (key, value) =>
      this.setOptionValue(key, value)

    const toggle = (key) =>
      this.setOptionValue(key, !this.state[key])

    const reset = (key) =>
      this.setOptionValue(key, DEFAULTS[key])

    return {
      ...this.state,
      set,
      reset,
      toggle,
    }
  }

  render () {
    return <Provider value={ this.getInterface() }>
      { this.props.children }
    </Provider>
  }
}


export default class SettingsConsumer extends Component {
  render () {
    return <Consumer>
      { this.props.render }
    </Consumer>
  }
}


export function withSettings (ComponentClass) {
  return class extends Component {

    static displayName = ComponentClass.displayName || ComponentClass.name

    component = createRef()

    render () {

      return <Consumer>{ context =>
        <ComponentClass
          { ...this.props }
          settings={ context }
          ref={ this.component }
        />
      }</Consumer>
    }

  }
}
