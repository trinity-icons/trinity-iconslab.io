// @flow
// @flow-runtime
import preview from '../../utils/Preview'
import type { Action } from '..'

export const UPDATE_SETTINGS  = 'UPDATE_SETTINGS'
export const TOGGLE_PANEL     = 'TOGGLE_PANEL'


export function updateProperty (key: string, value: any): Action<Object> {
  return {
    type: UPDATE_SETTINGS,
    payload: { [key]: value },

    affect: (state, dispatch) => {
      if (!state.preview.panelIsOpen)
        dispatch(togglePanelVisibility(true))
      preview.update(state.preview.toJSON())
    }
  }
}

export function togglePanelVisibility (visible: boolean): Action<{ visible: boolean }> {
  return {
    type: TOGGLE_PANEL,
    payload: { visible },
  }
}
