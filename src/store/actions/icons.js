
export const ADD_ICONS  = 'ADD_ICONS'


export function addIcons (icons) {
  return {
    type: ADD_ICONS,
    payload: { icons }
  }
}
