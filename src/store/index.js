// @flow
// @flow-runtime
import { composeWithDevTools } from 'redux-devtools-extension'
import { createStore, applyMiddleware } from 'redux'

import reducer from './reducer'
import initialState from './state'
import * as previewActions from './actions/preview'
import * as iconActions from './actions/icons'

export type { Store } from './state'


const effectLabel = 'affect'

const collateral = store => next => async action => {

  const result = await next(action)
  const affect = action[effectLabel]

  if (affect) {
    const state    = store.getState()
    const dispatch = store.dispatch.bind(store)
    await affect(state, dispatch)
  }
  return result
}


const composeEnhancers = composeWithDevTools({
  name: 'Trinity',
  actionCreators: {
    ...previewActions,
    ...iconActions,
  }
})

const middleware = [
  collateral,
]

const store = createStore(
  reducer,
  initialState,
  composeEnhancers(
    applyMiddleware(...middleware),

    // other store enhancers if any
  ))

export default store

export type Action<ActionName: string | null = null, Payload: Object = {}> = {|
  +type: ActionName,
  +payload: Payload,
  +affect?: Function,
|}
