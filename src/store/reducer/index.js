// @flow
// @flow-runtime
import { combineReducers } from 'redux'
import { List } from 'immutable'

import { UPDATE_SETTINGS, TOGGLE_PANEL } from '../actions/preview'
import state from '../state'

import type { Action } from '..'


type AddIconsAction       = Action<'ADD_ICONS',       { icons: Array<*> }>
type TogglePanelAction    = Action<'TOGGLE_PANEL',    { visible: boolean }>
type UpdateSettingsAction = Action<'UPDATE_SETTINGS', {}>

function icons (icons = state.icons, action: AddIconsAction) {

  switch (action.type) {
    case 'ADD_ICONS': {
      return List(action.payload.icons)
    }

    default: {
      return icons
    }
  }
}




function preview (settings = state.preview, action: TogglePanelAction | UpdateSettingsAction) {
  switch (action.type) {

    case UPDATE_SETTINGS: {
      return settings.merge(action.payload)
    }

    case TOGGLE_PANEL: {
      return settings.merge({ panelIsOpen: action.payload.visible })
    }

    default: {
      return settings
    }
  }
}

export default combineReducers({
  preview,
  icons,
})
