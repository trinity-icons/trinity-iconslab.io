// @flow
// @flow-runtime
import { List, Record } from 'immutable'
import type { RecordOf } from 'immutable'

export type PreviewSettingsSchema = {|
  iconStrokeWidth: number,
  showLabels: boolean,
  iconSize: number,
  iconColor: string,
  sortBy: string,
  filter: string,
  panelIsOpen: boolean,
|}


//: PreviewSettingsSchema
export const previewSchema = {
  iconStrokeWidth: 1.5,
  showLabels:      false,
  iconSize:        8,
  iconColor:       '#4b505a',
  sortBy:          'title',
  filter:          '',
  panelIsOpen:     false,
}

export const PreviewSettings = Record(previewSchema)



export const preview = PreviewSettings()
export const icons = new List()
export default {
  icons,
  preview,
}

export type Store = {
  icons: List<*>,
  preview: RecordOf<PreviewSettingsSchema>
}
