// @flow
// @flow-runtime
import makeRequest from 'axios'
import IconDescription from './models/Icon'

import type { IconType } from './models/Icon'

const url = (...path) =>
  `/symbols/${path.join('/')}`

const uri = {
  stats:   url('icon-stats.json'),
  symbols: url('icons.svg'),
  webfont: url('assets', 'iconfont.zip'),
  images:  url('assets', 'icons.zip'),
}



const stats = async (): Promise<Array<IconDescription>> =>
  formatStats(await request(uri.stats))

const symbols = (): Promise<string | null> =>
  request(uri.symbols)



async function request (url: string): Promise<* | null> {
  try {
    const response = await makeRequest(url)
    return response.data
  }
  catch (error) {
    // eslint-disable-next-line no-console
    console.error("request error", error)
    return null
  }
}


function formatStats (data: { [key: string]: IconType} | null): Array<IconDescription> {
  if (!data)
    return []

  let arr = []
  for (let [ key, item ] of Object.entries(data)) {

    // FIXME
    if (key.startsWith('_') || key.match(/empty(_\d+)?$/)) continue

    item.id = key
    arr.push(new IconDescription(item))
  }

  return arr.filter(IconDescription.isRegular)
}


module.exports = Object.defineProperties({}, {
  url:     { value: uri },
  stats:   { get:   stats },
  symbols: { get:   symbols },
})
