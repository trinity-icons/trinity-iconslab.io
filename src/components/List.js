// @flow
// @flow-runtime

import React, { Component, Fragment } from 'react'

import views from '../models/views'

import type { List } from 'immutable'
import type { IconType } from '../models/Icon'


type PropTypes = {|
  items?: List<IconType>,
|}


export default class List extends Component<PropTypes> {

  render () {
    return <ul className='icon list'>

      { this.props.items
        .map((child) =>
          <Fragment key={ child.id }>
            { views.get(child) }
          </Fragment>
        )
      }
    </ul>
  }
}


// const sortBy = (attr) =>
//   (a, b) =>
//     a[attr] > b[attr] ? 1 : 0
