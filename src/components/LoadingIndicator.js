// @flow
// @flow-runtime
import React, { Component } from 'react'


export default class LoadingIndicator extends Component<{||}> {

  render () {
    const style = {
      position: 'fixed'
    }
    return <span
      style={ style }
      className='loading' />
  }
}
