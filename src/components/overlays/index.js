
import React from 'react'
import Content from './OverlayContent'
import { prependElement } from '../../collateral/DOM'

import FillOverlay from './Overlay'
import DialogOverlay from './Dialog'


function withSeparateProps (fn) {

  const WrappedComponent = (properties) =>
    fn(getProps(properties))

  WrappedComponent.displayName = getDisplayName(fn)
  return WrappedComponent
}

const getProps = ({ onClose, children, ...props }) =>
  ({ onClose, children, props })

const getDisplayName = component =>
  ( component.displayName ||
    component.name ||
    component.constructor.name
  ).replace(/Base/g, '')

const hostNode =
  prependElement('section.overlays')

export const getHostElement = () =>
  hostNode



/**
 * Dialog window overlay
 * @function Dialog
 */

const BaseDialog = ({ props, onClose, children }) =>
  <DialogOverlay
    { ...props }
    onClose={ onClose }>

    <Content onRequestClose={ onClose }>
      { children }
    </Content>

  </DialogOverlay>


export const Dialog  = withSeparateProps(BaseDialog)


/**
 * Screen fill overlay
 * @function Modal
 */

const BaseModal = ({ props, onClose, children }) =>
  <FillOverlay
    { ...props }
    onClose={ onClose}>

    <Content onRequestClose={ onClose }>
      { children }
    </Content>

  </FillOverlay>


export const Modal   = withSeparateProps(BaseModal)
