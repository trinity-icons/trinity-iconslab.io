// @flow
// @flow-runtime
import React, { Component, Fragment } from 'react'
import {
  preventFurtherPropagation,
  withPropagationPrevented
} from '../../utils/events'

import type { Node } from 'react'

type ContentProps = {|
  children: Node,
  onRequestClose: Function,
|}


export default class ModalContent extends Component<ContentProps> {

  render () {
    return <Fragment>

      <span
        onClickCapture={ withPropagationPrevented(this.props.onRequestClose) }
        className='close'>
        x
      </span>

      <main onClickCapture={ preventFurtherPropagation }>
        { this.props.children }
      </main>

    </Fragment>
  }
}
