// @flow
// @flow-runtime
import BaseOverlay from './OverlayBase'


export default class ScreenOverlay extends BaseOverlay {
  static className = 'fill screen'
}
