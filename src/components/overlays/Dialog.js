/**
 * @module DialogOverlayComponent
 * @author tuomashatakka<tuomas.hatakka@gmail.com>
 * @flow
 */
import BaseOverlay from './OverlayBase'


export default class DialogOverlayComponent extends BaseOverlay {
  static className = 'dialog'
}
