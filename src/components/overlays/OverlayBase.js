// @flow
// @flow-runtime
import self from 'autobind-decorator'
import React, { Component } from 'react'
import { createPortal } from 'react-dom'
import { CompositeDisposable, Disposable } from 'event-kit'
import { element } from 'element-queries'
import {
  withPropagationPrevented,
  preventFurtherPropagation,
  addSingleDispatchListener,
  addClassNameSubscription
} from '../../utils/events'

import { getHostElement } from '.'

import type { Node } from 'react'


type Props = {|
  visible: boolean,
  children: Node,
  onClose: Function,
|}


type State = {|
  visible: boolean
|}


export default class BaseOverlay extends Component<Props, State> {

  static className = ''
  static TRANSITION_EVENT_NAME = 'animationend'

  subscriptions: CompositeDisposable
  element: HTMLElement

  element = element.create('div.overlay')
  state = { visible: false }

  static getDerivedStateFromProps (props: Props, state: State) {
    if (props.visible !== state.visible)
      return { visible: props.visible }
    return null
  }

  constructor (props: Props) {
    super(props)

    const trim = item =>
      item.trim()

    const applyClass = (className) =>
      this.element.classList.add(className)

    // Apply the static className
    // property for subclasses
    this.constructor
      .className
      .split(' ')
      .map(trim)
      .forEach(applyClass)
  }

  componentDidMount () {
    if (this.open)
      this.mountElement()
  }

  componentDidUpdate (props: Props, state: State) {
    if (this.state.visible !== state.visible) {
      if (this.open)
        this.mountElement()
      else
        this.unmountElement()
    }
  }

  componentWillUnmount () {
    this.unmountElement()
  }

  render () {
    return createPortal(
      this.content,
      this.element
    )
  }

  get content (): Node {
    return <div className='content'>
      { this.props.children }
    </div>
  }

  get open (): boolean {
    return this.state.visible
  }

  isOpen (): boolean {
    return this.open
  }

  attachElement () {
    const host = getHostElement()
    host.appendChild(this.element)
  }

  @self detachElement () {
    this.element.remove()
    this.clean()
  }

  mountElement () {

    const escapeHandler = (event) => {
      if (event.key === 'Escape') {
        this.props.onClose(event)
        return preventFurtherPropagation(event)
      }
    }

    const clickHandler = withPropagationPrevented(this.props.onClose)



    const callback = (event) => {

      const escapeSubscription = addSingleDispatchListener(
        window, 'keydown', escapeHandler, true)

      const clickSubscription = addSingleDispatchListener(
        document, 'click', clickHandler)

      this.subscribe(
        escapeSubscription,
        clickSubscription)

      if (typeof this.props.onOpen === 'function')
        this.props.onOpen(event)
    }

    this.subscribe(
      addSingleDispatchListener(this.element, this.constructor.TRANSITION_EVENT_NAME, callback),
      addClassNameSubscription(this.element, 'fade-in'))

    this.attachElement()
  }

  @self
  unmountElement () {
    this.subscribe(
      addSingleDispatchListener(this.element, this.constructor.TRANSITION_EVENT_NAME, this.detachElement),
      addClassNameSubscription(this.element, 'fade-out'))
  }

  subscribe (...subs: Array<Disposable>) {
    this.clean()
    this.subscriptions.add(...subs)
  }

  clean () {
    if (this.subscriptions)
      this.subscriptions.dispose()
    this.subscriptions = new CompositeDisposable()
  }

}
