// @flow
// @flow-runtime
import self from 'autobind-decorator'
import React, { Component, Fragment } from 'react'
import Modal from './Modal'
import type { Element } from 'react'
import type { IconType } from '../models/Icon'

type StateType = {|
  active: boolean,
|}

// const literals = [
//   'one',
//   'two',
//   'three',
//   'four',
//   'five',
//   'six',
//   'seven',
//   'eight',
//   'nine',
//   'ten'
// ]

const positional = [
  'first',
  'second',
  'third',
  'fourth',
  'fifth',
  'sixth',
  'seventh',
  'eighth',
  'ninth',
  'tenth'
]

const Word = props =>
  <Fragment>
    <span className={ `${positional[props.order] || 'nth'} word` }>
      { props.text }
    </span>
    <span className='whitespace'> </span>
  </Fragment>

const toWord = (word, key) =>
  <Word
    key={ key }
    text={ word }
    order={ key }
  />

const Heading = props =>
  <h2 className='title highlight'>

    { props.text
      .split(/\s+/g)
      .map(toWord)
    }

  </h2>


export default class Icon extends Component<IconType, StateType> {

  state = { active: false }

  get name (): string {
    return this.props.title.replace(/-+/g, ' ')
  }

  get href (): string {
    if (!this.props.id)
      throw new ReferenceError(`Cannot render an Icon component without an id prop`)
    return `#${this.props.id.replace(/\./g, '-')}`
  }

  @self
  toggle () {
    this.setState({ active: !this.state.active })
  }

  get icon (): Element<*> {
    return <svg className='sprite'>
      <use href={ this.href } />
    </svg>
  }

  render () {

    return <li className='icon entry'>

      <article onClick={ this.toggle }>
        <header>
          { this.name }
        </header>
        { this.icon }
      </article>

      <Modal
        onClose={ this.toggle }
        visible={ this.state.active }>

        <Heading text={ this.name } />
        <section className='information'>
          Key code: \\fxdxd007
        </section>
        <section className='instructions'>
          (tabs)
          <button>svg</button>
          <button>font</button>
        </section>
        <section className='preview'>
          { this.icon }
        </section>

      </Modal>

    </li>
  }

}
