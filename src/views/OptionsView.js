// @flow
// @flow-runtime
import React, { Component, createRef } from 'react'
import { CompositeDisposable } from 'event-kit'
import { connect } from 'react-redux'

import { updateProperty, togglePanelVisibility } from '../store/actions/preview'
import { addListener } from '../utils/events'

import type { Node } from 'react'
import type { Store } from '../store'

type PropTypes = {|
  visible: boolean,
  settings: Object,
|}


type FieldProps<Value> = {
  onChange: Function,
  value: Value,
}

type ColorFieldProps = FieldProps<string>

type TextFieldProps = FieldProps<string> & {
  focus: boolean,
}

type SliderFieldProps = FieldProps<number> & {
  range: [ number, number ]
}

type ToggleFieldProps = FieldProps<boolean> & {
}


const STATES = [
  'hidden',
  'expanded',
]


const onChange = (props: FieldProps): (SyntheticEvent<*> => void) => event =>
  props.onChange(event.target.value)


class SearchField extends Component<TextFieldProps> {

  element = createRef()

  componentWillReceiveProps (props) {
    if (props.focus)
      this.element.current.focus()
  }

  render () {
    return <input
      type='search'
      ref={ this.element }
      value={ this.props.value }
      onBlur={ this.props.onBlur }
      onFocus={ this.props.onFocus }
      onChange={ onChange(this.props) }
    />
  }
}



const Checkbox = (props: ToggleFieldProps) =>
  <input
    type='checkbox'
    checked={ props.value }
    onChange={ onChange(props) }
  />


const Color = (props: ColorFieldProps) =>
  <input
    type='color'
    value={ props.value }
    onChange={ onChange(props) }
  />


const Slider = (props: SliderFieldProps) =>
  <input
    type='range'
    step={ 0.5 }
    min={ props.range[0] }
    max={ props.range[1] }
    value={ props.value }
    onChange={ onChange(props) }
  />


@connect(connectProperties, connectActions)
export default class OptionsView extends Component<PropTypes, { focus: null | string }> {

  state = {
    focus: null
  }

  searchField: Node
  searchField = createRef()

  focusSearchField () {
    this.props.toggleVisibility(true)
    if (document.activeElement !== this.searchField.current.element.current)
      this.searchField.current.element.current.focus()
  }

  componentDidMount () {

    const onKey = (event) => {
      if (event.key === 'Escape')
        this.props.setFilter('')
      else if (event.key.length === 1)
        this.focusSearchField()
    }

    this.subscriptions = new CompositeDisposable()
    this.subscriptions.add(
      addListener(document, 'keydown', onKey, true)
    )
  }

  componentWillUnmount () {
    this.subscriptions.dispose()
  }

  render () {

    const settings = this.props.settings
    const state    = STATES[this.props.visible ? 1 : 0]

    const toggleVisibility = () =>
      this.props.toggleVisibility(!this.props.visible)

    return <aside className={ `preview-options ${state}` }>

      <button
        className='only-icon display'
        onClick={ toggleVisibility }>
        <svg>
          <use href='#regular-cog-filled' />
        </svg>
      </button>

      <section className={ `controls ${state}` }>

        <label className={ this.state.focus === 'search' ? 'active' : 'inactive'}>
          <h3>Filter</h3>
          <SearchField
            ref={ this.searchField }
            value={ settings.filter }
            focus={ settings.filter.length > 0 }
            onBlur={ () => this.setState({ focus: null })}
            onFocus={ () => this.setState({ focus: 'search' })}
            onChange={ this.props.setFilter }
          />
        </label>

        <label>
          <h3>Labels</h3>
          <Checkbox
            value={ settings.showLabels }
            onChange={ () => this.props.toggleLabels(!settings.showLabels) }
          />
        </label>

        <label>
          <h3>Stroke weight</h3>
          <Slider
            range={[ 1, 8 ]}
            value={ settings.iconStrokeWidth }
            onChange={ this.props.setIconStrokeWidth }
          />
        </label>

        <label>
          <h3>Icon size</h3>
          <Slider
            range={[ 3, 33.3 ]}
            value={ settings.iconSize }
            onChange={ this.props.setIconSize }
          />
        </label>

        <label>
          <h3>Color</h3>
          <Color
            value={ settings.iconColor }
            onChange={ this.props.setIconColor }
          />
        </label>

      </section>

    </aside>
  }
}


function connectProperties (state: Store): { settings: Object } {
  return {
    settings: state.preview,
    visible: state.preview.panelIsOpen,
  }
}


function connectActions (dispatch: Function): Object {

  const toggleVisibility = (value) =>
    dispatch(togglePanelVisibility(value))

  const toggleLabels = (state) =>
    dispatch(updateProperty('showLabels', state))

  const setIconSize = (value) =>
    dispatch(updateProperty('iconSize', value))

  const setIconColor = (value) =>
    dispatch(updateProperty('iconColor', value))

  const setIconStrokeWidth = (value) =>
    dispatch(updateProperty('iconStrokeWidth', value))

  const setFilter = (value) =>
    dispatch(updateProperty('filter', value))

  return {
    toggleLabels,
    setIconSize,
    setIconColor,
    setIconStrokeWidth,
    setFilter,
    toggleVisibility,
  }
}
