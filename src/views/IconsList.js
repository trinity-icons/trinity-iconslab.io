// @flow
// @flow-runtime
import React, { Component } from 'react'
import { connect } from 'react-redux'

import api from '../api'
import Listing from '../components/List'
import Loading from '../components/LoadingIndicator'
import preview from '../utils/Preview'
import { addIcons } from '../store/actions/icons'
import { appendSymbolsElement, showExceptionElement } from '../collateral/DOM'

import type { List } from 'immutable'
import type { IconType } from '../models/Icon'


type Props = {|
  items: ?List<IconType>,
  updateItems: (Array<IconType>) => void,
|}


@connect(connectProperties, connectActions)
export default class IconsList extends Component<Props> {

  constructor (props: Props) {
    super(props)
    load()
      .catch(showExceptionElement)
      .then(this.props.updateItems)
      .then(updatePreview)
  }

  render () {
    if (!this.props.items.size)
      return <Loading />
    return <Listing items={ this.props.items } />
  }
}


function connectProperties (state: { items?: List<IconType> }) {
  const filterIcon = createFilterFn(state.preview.filter)
  const order = createSortFn(state.preview.sortBy)
  return {
    items: state.icons
      .filter(filterIcon)
      .sort(order)
  }
}


function connectActions (dispatch: Action<{ icons: Array<IconType> }> => void) {

  const updateItems = items =>
    dispatch(addIcons(items))

  return { updateItems }
}


function createFilterFn (query) {
  const parts = cleanQuery(query)

  const matchAll = term =>
    parts.reduce((tot, c) => tot && term.includes(c), true)

  return icon => matchAll(icon.title)
}


function createSortFn (property) {
  return (a, b) => b[property] > a[property] ? 0 : 1
}


async function load () {
  let symbolsRequest = fetchSymbols()
  let statsRequest   = fetchStats()
  let [ icons, ]      = await Promise.all([ statsRequest, symbolsRequest ])
  return icons
}

const cleanQuery = (query: string): Array<string> => query
  .replace(/([^\w])+/g, ' ')
  .trim()
  .toLowerCase()
  .split(' ')
  .filter(item => item)

const updatePreview = (): any =>
  preview.refresh()

const fetchStats = (): Promise<Object> =>
  api.stats

const fetchSymbols = async (): Promise<HTMLElement> =>
  appendSymbolsElement(await api.symbols)
