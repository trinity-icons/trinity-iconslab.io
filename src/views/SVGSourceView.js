/**
 * @module SVGSourceView
 * @author tuomashatakka<tuomas.hatakka@gmail.com>
 * @flow
 */
import self from 'autobind-decorator'
import React, { PureComponent, createRef } from 'react'
import { Dialog } from '../components/overlays'
import Loading from '../components/LoadingIndicator'
import { getSymbolsElement } from '../collateral/DOM'
import api from '../api'
// import copy from 'copy-to-clipboard'

let _svg = ''

export function getSymbolsSource (): string {
  if (_svg)
    return _svg
  const src = getSymbolsElement()
  if (src)
    _svg = src.innerHTML.trim()
  return _svg
}

function copyToClipboard (str) {

  function listener(e) {
    e.clipboardData.setData("text/plain", str)
    e.preventDefault()
  }

  document.addEventListener("copy", listener)
  document.execCommand("copy")
  document.removeEventListener("copy", listener)
}

type Props = {
  visible: boolean,
  onClose: Function,
  onOpen:  Function,
}

type StateType = {
  html:    string,
  copied:  boolean,
  loading: boolean,
}


export default class SVGSourceView extends PureComponent<Props, StateType> {

  field   = createRef()
  element = createRef()

  state = {
    html:    '',
    copied:  false,
    loading: true,
  }

  @self onOpen () {
    if (!this.state.html)
      this.updateContent()

    else
      this.setState({ loading: false })
  }

  async updateContent () {
    const html = await api.symbols
    console.warn("api symbol response:", html)

    this.setState({
      html,
      copied:  false,
      loading: false,
    })

  }

  get content (): string {
    return this.state.html
  }

  @self onClose () {
    this.setState({
      copied:  false,
    })
    this.props.onClose()
  }

  render () {

    const redirect = () => {
      const url = location.origin + api.url.symbols
      const win = window.open(url, '_blank')
      if (win) win.focus()
      else window.location.href = url
    }

    const copyContents = e => { // eslint-disable-line

      console.warn("Focused textarea", e)
      const el = this.field.current

      el.select()
      el.selectionStart = 0
      el.selectionEnd   = el.length
      let success = false

      try {
        copyToClipboard(this.content)
        success = true
      }
      catch (e) {
        console.error(e)
      }

      try {
        document.execCommand('copy')
        success = true
      }
      catch (e) {
        console.error(e)
      }

      e.preventDefault()
      e.nativeEvent.preventDefault()
      if (success)

        setTimeout(() => this.setState({ copied: true }), 800)


      if (window.getSelection) {

        const pr = this.element.current
        const selection = getSelection()
        let selectionSuccess = false

        try {
          selection.selectAllChildren(pr)
          selectionSuccess = true }

        catch (e) {
          console.error(e) }

        try {
          const range = selection.rangeCount && selection.getRangeAt(0)
          if (range) {

            range.selectNodeContents
              ? range.selectNodeContents(pr)
              : range.selectNode(pr)
            selectionSuccess = true }}

        catch (e) {
          console.error(e) }

        if (!selectionSuccess) {
          pr.focus()
          window.prompt(
            "Automatic copy failed. Please copy the text below.",
            this.content
          )
        }
      }
    }

    const focusContents = () =>
      this.field.current.focus()

    return <Dialog
      onOpen={  this.onOpen }
      onClose={ this.onClose }
      visible={ this.props.visible }>

      { this.state.loading
        ? <Loading />
        : <div>

          <aside className='toolbar'>

            <button
              className='button'
              onMouseDownCapture={ focusContents }>
              Copy
            </button>

            <button
              className='button'
              onMouseDownCapture={ redirect }>
              Download
            </button>

          </aside>

          { this.state.copied && <h1>Copioitu!</h1> }

          <pre
            className='pippeli'
            ref={ this.element }>
            { this.content }
          </pre>

          <textarea
            ref={ this.field }
            value={ this.content }
            onFocus={ copyContents }
            className='cippeli'
          />
        </div>
      }

    </Dialog>
  }

}
