/* eslint-disable block-padding/functions */
import { Disposable, CompositeDisposable } from 'event-kit'


export function makeSubscribable (constructor) {

  return function () {

    constructor.apply(this, arguments)
    const mount   = this.componentDidMount
    const unmount = this.componentWillUnmount

    this.componentDidMount = () => {
      this.subscriptions = new CompositeDisposable()
      mount()
    }

    this.componentWillUnmount = () => {
      unmount()
      this.subscriptions.dispose()
    }
  }
}

export function addListener (element, eventName, handler, pre = false) {
  const unbind = () =>
    element.removeEventListener(eventName, handler, pre)
  element.addEventListener(eventName, handler, pre)
  return new Disposable(unbind)
}


export function addClassNameSubscription (element, className) {
  const remove = () =>
    element.classList.remove(className)
  element.classList.add(className)
  return new Disposable(remove)
}

export const withDefaultPrevented = fn => event =>
  event.preventDefault()
  || fn(event)
  || false


export const withPropagationPrevented = (fn: Function) => function (event) {
  preventFurtherPropagation(event)
  fn(event)
  return true
}


export function preventFurtherPropagation (event) {
  if (typeof event.preventDefault === 'function')
    event.preventDefault()
  if (typeof event.stopPropagation === 'function')
    event.stopPropagation()
  if (typeof event.stopImmediatePropagation === 'function')
    event.stopImmediatePropagation()
  if (event.nativeEvent)
    return preventFurtherPropagation(event.nativeEvent)
  return true
}


export function addSingleDispatchListener (node, eventName, callback, pre) {

  const boundCallback = (...args) => {
    subscription.dispose()
    return callback(...args)
  }
  const subscription = addListener(node, eventName, boundCallback, pre)
  return subscription
}
