import { appendPreviewStyleElement, applyGlobalProperty } from '../collateral/DOM'
import { DEFAULT_PROPERTIES } from '../models/settings'


class PreviewManager {
  properties: Map<string, any>

  constructor () {
    this.properties = new Map(DEFAULT_PROPERTIES)
    for (let [ name, ] of DEFAULT_PROPERTIES) {
      const get = this.read.bind(this, name)
      const set = this.write.bind(this, name)
      Object.defineProperty(this, name, { get, set })
    }
  }

  read (key: string) {
    if (this.properties.has(key))
      return this.properties.get(key)
    return null
  }

  write (key: string, value: any) {
    this.update({ [key]: value })
  }

  update (valuesMap: { [key: string]: any } = {}) {
    for (let attr in valuesMap)
      this.properties.set(attr, valuesMap[attr])
    this.refresh()
  }


  refresh () {
    const styles = this.serialize()
    appendPreviewStyleElement(styles)
    applyGlobalProperty('preview-show-labels', this.showLabels)
  }


  serialize (): Object {
    return {

      '.icon.entry': {
        'color':          this.iconColor,
        'font-size':   `${this.iconSize}rem`,
      },

      '.outline, .fill': {
        'stroke-width': this.iconStrokeWidth + 'px',
      },

    }
  }

  toJSON (): Object {
    return this.serialize()
  }

}


export default new PreviewManager()
