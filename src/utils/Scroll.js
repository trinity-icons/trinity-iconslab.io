// @flow
import self from 'autobind-decorator'


const scrollers: WeakMap<HTMLElement, Scroll> = new WeakMap()


function getScrollHeight (element: typeof window | HTMLElement): number | null {
  if (!(element instanceof HTMLElement)) element = element.document

  while (!element.scrollHeight) {
    element = element.firstElementChild
    if (!element) return null
  }
  let scrollHeight = element.scrollHeight
  return scrollHeight ? scrollHeight - element.clientHeight : null
}


export function scrollToEnd (element?: Element = window): Scroll | null {
  let scrollHeight = getScrollHeight(element)

  if (typeof scrollHeight === 'number') return scrollToPosition(scrollHeight)
  return null
}


export default function scrollToPosition (targetPosition: number, element?: HTMLElement | Window = window): Scroll {
  let scroll: Scroll | void

  if (scrollers.has(element)) {
    scroll = scrollers.get(element)
    scroll.stop()
  }
  if (!scroll) {
    scroll = new Scroll(element)
    scrollers.set(element, scroll)
  }

  console.log("Scrolling", scroll)
  scroll.scrollTo(targetPosition)
  return scroll
}


class Scroll {
  element: HTMLElement
  _targetPosition: number | null
  scrollHeight: number | null
  terminated: ?boolean

  static MAXIMUM_ITERATIONS: number = 600
  static MINIMUM_SPEED: number = 0.25
  static SCROLL_SPEED: number = 10
  static THRESHOLD: number = 6

  constructor(element?: HTMLElement = window) {
    if (typeof element.scrollTo !== 'function')
      throw new ReferenceError(`Invalid element: element.scrollTo is not a function.`)
    this.element = element
    this.scrollHeight = getScrollHeight(element)
    this._targetPosition = this.currentPosition
  }

  set targetPosition(targetPosition: number) {
    let scrollHeight = getScrollHeight(this.element)
    if (!scrollHeight) return
    if (scrollHeight < targetPosition) return
    if (scrollHeight > 0) {
      while (targetPosition < scrollHeight) targetPosition += scrollHeight
      while (targetPosition > scrollHeight) targetPosition -= scrollHeight
      this.scrollHeight = scrollHeight
    }

    this._targetPosition = targetPosition
  }

  get targetPosition(): number | null {
    return this._targetPosition
  }

  set currentPosition(position: number) {
    this.element.scrollTo(0, position)
  }

  get currentPosition(): number {
    if (typeof this.element.scrollY === 'number') return Math.floor(this.element.scrollY)
    else if (typeof this.element.scrollTop === 'number') return Math.floor(this.element.scrollTop)
    return 0
  }

  get positionDelta (): number {
    if (this.targetPosition === null) return 0
    return this.targetPosition - this.currentPosition
  }

  get nextPosition (): number {
    let diff = this.positionDelta
    let y = this.currentPosition
    let f = diff >= 0 ? 1 : -1
    let speed = Math.pow(Math.abs(diff) / 100, 1.1) * Scroll.SCROLL_SPEED
    let dy = f * Math.max(parseFloat(speed), Scroll.MINIMUM_SPEED)

    if (this.targetPosition === null) return y
    else if (this.targetPosition < y) return y - dy
    else return y + dy
  }

  shouldScroll () {
    if (this.terminated || this.iterations < 1) return false
    return Math.abs(this.positionDelta) > Scroll.THRESHOLD
  }

  @self
  run () {
    if (this.shouldScroll()) {
      let next = this.nextPosition
      if (next === this.currentPosition) this.stop()
      this.currentPosition = next
      this.iterations--

      requestAnimationFrame(() => this.run())
    }
    else
      this.stop()
  }

  @self
  stop () {
    this.terminated = true
    window.removeEventListener('wheel', this.stop)
  }

  scrollTo (targetPosition: number) {
    this.terminated = false
    this.targetPosition = targetPosition
    window.addEventListener('wheel', this.stop)
    this.iterations = Scroll.MAXIMUM_ITERATIONS
    this.run()
  }
}
