// @flow
// @flow-runtime
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { AppContainer } from 'react-hot-loader'

import store from './store'

import type { Node } from 'react'


const getRootNode = (): HTMLElement | false =>
  document.body instanceof HTMLElement
  && document.body.appendChild(
    document.createElement('main')
  )

const host = getRootNode()

const mount = (Application: Node): Node | false =>
  host && render(
    <Provider store={ store }>
      <AppContainer>
        <Application />
      </AppContainer>
    </Provider>,
    host)


mount(require('./Application').default)

if (module.hot)

  module.hot.accept('./Application', () =>
    mount(require('./Application').default))
