
const DESCRIPTOR_EXPRESSION = /([\w\d-_]*)((?:#|\.)[\w\d-_]+)*/


const isClass = identifier =>
  identifier && identifier.startsWith('.')


const isKey = identifier =>
  identifier && identifier.startsWith('#')


const removePrefix = identifier => identifier
  ? identifier.substr(1)
  : null


const include = {
  removePrefix,
  getClassNames: extract('classNames'),
  getTagName:    extract('tagName'),
  getId:         extract('id'),
}


function parseDescriptor (descriptor) {
  const [ , tagName, ...identifiers ] = descriptor.match(DESCRIPTOR_EXPRESSION)
  const classNames = identifiers.filter(isClass).map(removePrefix)
  const id         = identifiers.find(isKey)

  return { id, tagName, classNames }
}


function extract (propertyName) {

  return descriptor => parseDescriptor(descriptor)[propertyName]
}

module.exports = Object.assign(
  parseDescriptor,
  include)
