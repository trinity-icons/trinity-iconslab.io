
module.exports = updateElementAttributes


const attributeResolvers = {
  class:   resolveClass,
  content: resolveContent
}

const isIterable = (obj)=>
  obj && obj !== null &&
  typeof obj[Symbol.iterator] === 'function'

const clear = element =>
  element.innerHTML = ''


function resolveContent (element, value) {
  if (!value)
    clear(element)
  else if (typeof value === 'string')
    element.innerHTML = value
  else if (value instanceof HTMLElement) {
    clear(element)
    element.appendChild(value)
  }
  else if (isIterable(value)) {
    clear(element)
    for (let child of value)
      element.appendChild(child)
  }
}


function resolveClass  (element, value) {
  if (value instanceof Array)
    value = value.join(' ')
  if (typeof value !== 'string')
    throw new TypeError(`Invalid class name provided for an element of type ${element.tagName.toLowerCase()}.
      The element's class attribute should be provided either as a list of strings or a string consisting of
      whitespace-delimited list of discrete class names.`)
  element.setAttribute('class', value)
}


function updateElementAttributes (element, attributes = {}) {
  for (let [ name, value ] of Object.entries(attributes))
    updateAttribute(element, name, value)
  return element
}


function updateAttribute (element, name, value = null) {
  const update = attributeResolvers[name]
  if (typeof update === 'function')
    update(element, value)
  else if (value === null)
    element.removeAttribute(name)
  else
    element.setAttribute(name, value)
  return element
}
