const updateElementAttributes = require('./attribute')
const selector = require('./selector')

const include = {
  get:          getElement,
  create:       createElement,
  getOrCreate:  getOrCreateElement,
}


function createElement (descriptor) {
  const { id, tagName, classNames } = selector(descriptor)
  const element = document.createElement(tagName || 'div')
  const attrs   = {
    id:    selector.removePrefix(id),
    class: classNames,
  }
  updateElementAttributes(element, attrs)
  return element
}


function getElement (descriptor) {
  return document.querySelector(descriptor)
}


function getOrCreateElement (descriptor) {
  const element = getElement(descriptor)
  return element || createElement(descriptor)
}


module.exports = Object.assign(
  createElement,
  include)
