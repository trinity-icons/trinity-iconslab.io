
// const HotModuleReplacementPlugin = require('webpack/lib/HotModuleReplacementPlugin')
const BuildStatusPlugin = require('build-status-webpack-plugin')
const DefinePlugin      = require('webpack/lib/DefinePlugin')
const paths             = require('../paths')
const loaders           = require('./loaders')


const mode = process.env.NODE_ENV || 'development'

const extensions = [ '.js', '.json', '.jsx' ]

const buildPath  = paths.build.root

const entry = [

  // 'webpack-hot-middleware/client?path=/delta',
  // 'webpack-hot-middleware/client?path=' + paths.reload,

  'react-hot-loader/patch',
  "./src"
]

const rules = [
  loaders.transpiler,
  loaders.styles,
]

const generateLibraryAlias = reduceArrayToObject(paths.library.named)

const alias = paths.library
  .list()
  .reduce(generateLibraryAlias, {})


module.exports = {
  mode,
  entry,
  module: { rules },
  resolve: {
    extensions,
    alias,
  },
  devtool: 'cheap-module-source-map',

  plugins: [

    // new HotModuleReplacementPlugin(),

    new BuildStatusPlugin({}),
    new DefinePlugin({
      'global.RUNTIME':       JSON.stringify('client'),
      'process.env.NODE_ENV': JSON.stringify(mode),
    }),
  ],

  output: {
    path:                   buildPath,
    library:                'trinity',
    filename:               '[name].js',
    publicPath:             '/',
    libraryTarget:          'umd2',
    jsonpFunction:          'run',
    hotUpdateFunction:      'update',
    hotUpdateMainFilename:  'update.[hash].json',
    hotUpdateChunkFilename: 'update.[hash].[id].js',
  },

  devServer: {
    publicPath:         '/',
    logLevel:           'silent',
    historyApiFallback: true,
    serverSideRender:   true,
    compress:           false,
    quiet:              true,
    hot:                true,
    port:               3000,
  }

}


function reduceArrayToObject (map) {

  return (obj, entry) => {
    const addon = { [ entry ]: map(entry) }
    return Object.assign({}, obj, addon)
  }
}
