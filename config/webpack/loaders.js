
const LessUtilitiesPlugin = require('less-plugin-util')
const LessListsPlugin     = require('less-plugin-lists')
const autoprefixer        = require('autoprefixer')


const less_loader = {
  loader: 'less-loader',
  options: {
    plugins: [ new LessListsPlugin(), LessUtilitiesPlugin ]
  }
}

const style_loader = {
  loader: 'style-loader',
  options: { hmr: true }
}

const postcss_loader = {
  loader: 'postcss-loader',
  options: {
    sourceMap: true,
    plugins: [ autoprefixer() ],
  }
}


module.exports = {

  transpiler: {
    exclude: /node_modules/,
    test:    /\.jsx?$/,
    loader: 'babel-loader',
  },

  styles: {
    exclude: /node_modules/,
    test:    /\.(le|c)ss$/,
    use:     [
      style_loader,
      'css-loader',
      postcss_loader,
      less_loader ],
  },

  linter: {
    exclude: /node_modules/,
    test:    /\.jsx?$/,
    loader: 'eslint-loader',
  },

}
