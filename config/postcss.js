const autoprefixer = require('autoprefixer')

module.exports = {
  plugins: {
    autoprefixer: autoprefixer({
      browsers: [ 'last 3 versions', '> 1%' ]
    }),
  }
}
