const { readdirSync: list } = require('fs')
const { resolve } = require('path')
const project_root = resolve(__dirname, '..')


const config = {
  root:    path('config'),
  postcss: path('config/postcss.js'),
}

const build = {
  root:    path('dist'),
}

const library = {
  root:    path('modules'),
  list:    readLibraries,
  named:   getLibraryPathByName,
}

module.exports = {
  config,
  build,
  library,
}


function path (...path) {
  return resolve(project_root, ...path)
}

function readLibraries () {
  return list(library.root)
}

function getLibraryPathByName (libraryName) {
  return resolve(project_root, 'modules', libraryName)
}
