import fs from 'fs'
import webpack from 'webpack'
import middleware from 'koa-webpack'
import config from '../../config/webpack'
import paths from '../meta'

function defineRoute (url, view) {

  const match = (path) =>
    new RegExp(url, 'ig').test(path)

  return async function (context, next) {

    const matches = match(context.request.url)
    const delim   = matches ? "≠" : "≈"

    console.log("↘ Request url", delim, url)

    if (matches)
      return view(context)
    else
      return next(context)
  }
}

function servePageMiddleware (context) {
  if (context.response.status === 404) {
    context.response.type = 'html'
    context.response.body = fs.createReadStream(paths.index)
  }
  return context
}

function compileBundleMiddleware () {
  const compiler   = webpack(config)
  const options    = {
    compiler,
    dev: config.devServer,
    hot: {
      logLevel:   'silent',
      log:        false, // eslint-disable-line
      path:       '/delta',
      heartbeat:  10 * 1000
    }
  }

  return middleware(options)
}

function defineReadRoute (url, path) {

  const type = url.split('.').pop()

  const view = function (context) {
    context.response.type = type
    context.response.body = fs.createReadStream(path)
    return context
  }

  return defineRoute(url, view)
}

const catalog   = defineReadRoute('/symbols/icon-stats.json', `${paths.catalog}/icon-stats.json`)
const symbols   = defineReadRoute('/symbols/icons.svg', `${paths.catalog}/icons.svg`)
const iconfont  = defineReadRoute('/symbols/assets/iconfont.zip', `${paths.catalog}/assets/iconfont.zip`)
const icons     = defineReadRoute('/symbols/assets/icons.zip', `${paths.catalog}/assets/icons.zip`)

const routes = [
  catalog,
  symbols,
  iconfont,
  icons,
]

export default Object.defineProperties({}, {
  page: { value: servePageMiddleware },
  bundle: { get: compileBundleMiddleware },
  routes: { value: routes },
})
